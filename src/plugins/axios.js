import Vue from 'vue';

export default function ({ $cookies, $axios, redirect }) {
    $axios.onRequest(config => {
        if(config.url != 'login') {
            let token = $cookies.get('gym_authToken');
            if(token == null)
                redirect('login');
            else 
                $axios.defaults.headers.common = {'Authorization': `Bearer ${token}`};
        }
        $nuxt.$emit('loading-show');
    });
    $axios.onResponse(config => {
      if(config.config.method != "get") {
        if(config.status == 200) {
            if(config.data.status == 'error') {
                if(config.data.error.substring(0,6) === 'Token ') {
                    Vue.notify({
                        'group': 'main-notifications',
                        'type': 'warn',
                        'title': "Sesión terminada",
                        'text': "Por favor inicie sesión nuevamente"
                    });
                    setTimeout(function() {
                        redirect('login');
                    }, 3000);
                } else {
                    Vue.notify({
                        'group': 'main-notifications',
                        'type': 'warn',
                        'title': "Acción terminada con errores",
                        'text': config.data.error
                    });
                }
            } else {
                Vue.notify({
                    'group': 'main-notifications',
                    'type': 'success',
                    'title': "Acción terminada exitosamente",
                    'text': "Su acción fue terminada con éxitos"
                });
            }
        } else {
            Vue.notify({
                'group': 'main-notifications',
                'type': 'error',
                'title': "Error interno",
                'text': "Se produjo un error en el servidor. Por favor inténtelo más tarde"
            });
        }
      }
      $nuxt.$emit('loading-hide');
    })
  
    $axios.onError(error => {
        Vue.notify({
            'group': 'main-notifications',
            'type': 'error',
            'title': "Error interno",
            'text': "Se produjo un error en el servidor. Por favor inténtelo más tarde. Si el error persiste, comuníquese con el administrador"
        });
        $nuxt.$emit('loading-hide');
    })
}