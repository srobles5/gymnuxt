import Vue from 'vue';

const validValues = {
    name: 'ValidValues',
    valid(arr) {
        if(typeof arr == 'object') {
            const keys = Object.keys(arr);
            for (let i = 0; i < keys.length; i++) {
                if(!this.validVal(arr[keys[i]])) {
                    console.log(arr[keys[i]]);
                    this.notify();
                    return false;
                }
            }
        } else if(typeof arr == 'array') {
            for (let i = 0; i < arr.length; i++) {
                if(!this.validVal(arr[i])) {
                    console.log(arr[i]);
                    this.notify();
                    return false;
                }
            }
        } else {
            if(!this.validVal(arr)) {
                console.log(arr);
                this.notify();
                return false;
            }
        }
        return true;
    },
    validVal(val) {
        return (val != undefined && val != null && val != '') || (typeof val == 'boolean') || (typeof val == 'number' && val == 0);
    },
    notify() {
        Vue.notify({
            'group': 'main-notifications',
            'type': 'warn',
            'title': "Atención",
            'text': "Favor llenar todos los campos requeridos"
        });
    }
}

export default ({ app }, inject) => {
    inject('validValues', validValues);
}