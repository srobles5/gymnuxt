window.onload = function() {
    let notifyBoxTrigger = document.getElementById('notifyBoxTrigger');
    notifyBoxTrigger.addEventListener('click', notifyBoxTriggerClick);
    document.getElementById('notifyContainer').addEventListener('click', stopPropagation);

    function notifyBoxTriggerClick(e) {
        e.stopPropagation();
        document.getElementById('notifyContainer').classList.toggle('active');
    }

    let body = document.getElementById('body');
    body.addEventListener('click', notifyBoxBodyClick);

    function notifyBoxBodyClick() {
        if(document.getElementById('notifyContainer').classList.contains('active')) {
            document.getElementById('notifyContainer').classList.remove('active');
        }
    }

    function stopPropagation(e) {
        e.stopPropagation();
    }
}