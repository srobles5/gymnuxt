window.onload = function() {
    let peopleCardsBox = document.getElementById('peopleCardsBox');
    peopleCardsBox.addEventListener('click', modalOpenClick);
    // peopleCardsBox.dispatchEvent(new Event('click'));

    function modalOpenClick(e) {
        e.stopPropagation();
        document.getElementById('modal01').classList.add('active');
    }

    let modal01 = document.getElementById('modal01');
    modal01.addEventListener('click', modalCloseClick);

    function modalCloseClick(e) {
        e.stopPropagation();
        document.getElementById('modal01').classList.remove('active');
    }

    let modal_box = modal01.firstElementChild;
    modal_box.addEventListener('click', stopPropagation);

    function stopPropagation(e) {
        e.stopPropagation();
    }

    document.addEventListener('keydown', function(event){
        if(event.key === "Escape"){
            if(modal01.classList.contains('active'))
                modal01.dispatchEvent(new Event('click'));
        }
    });
}