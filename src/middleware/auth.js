export default function ({ app, redirect }) {
    const token = app.$cookies.get('gym_authToken');
    if (!token) {
      return redirect('/login');
    }
}